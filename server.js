const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const app = express();
const authRoutes = require('./routes/auth.routes');
const trucksRoutes = require('./routes/trucks.routes');
const usersRoutes = require('./routes/users.routes');
const loadRoutes = require('./routes/load.routes');
require('dotenv').config();
const port = process.env.PORT || 8080;
const uri = process.env.DB_URI;
const {authVerifyToken} = require('./middleware/auth.middleware');

app.use(express.urlencoded({extended: false}));
app.use(express.json());
// app.use(cors);
app.use(morgan('tiny'));
// app.use(express.static(path.join(__dirname, 'build')));

const connectToDb = async (uri) => {
    try {
        await mongoose.connect(uri);
        console.log('CONNECTED TO MONGO')
    } catch (e) {
        console.error('Failed to connect to MongoDB', e);
    }
}

// app.get('/api/auth', (req, res) => res.status(200).json({message: 'Hello'}));
app.use('/api/auth', authRoutes);
app.use('/api/trucks', authVerifyToken, trucksRoutes);
app.use('/api/loads', authVerifyToken, loadRoutes);
app.use('/api/users', authVerifyToken, usersRoutes);

// app.get('/', (req, res) => {
//     res.sendFile(path.join(__dirname, 'build', 'index.html'))
// });

connectToDb(uri)
    .then(() => app.listen(port, () => console.log(`Server is up and running on port ${port}`)));
