const ROLES = {
    SHIPPER: 'SHIPPER',
    DRIVER: 'DRIVER'
};

const TRUCK_TYPES = {
    SPRINTER: 'SPRINTER',
    SMALL_STRAIGHT: 'SMALL STRAIGHT',
    LARGE_STRAIGHT: 'LARGE STRAIGHT'
};

const TRUCK_STATUSES = {
    IN_SERVICE: 'IS',
    ON_LOAD: 'OL'
};

const TRUCK_TYPES_SIZES = {
    SPRINTER: {
        length: 300,
        height: 250,
        width: 170
    },
    SMALL_STRAIGHT: {
        length: 500,
        height: 250,
        width: 170
    },
    LARGE_STRAIGHT: {
        length: 700,
        height: 350,
        width: 200
    },
};

const TRUCK_PAYLOADS = {
    SPRINTER: 1700,
    SMALL_STRAIGHT: 2500,
    LARGE_STRAIGHT: 4000,
}


const LOAD_STATUSES = {
    NEW: 'NEW',
    POSTED: 'POSTED',
    ASSIGNED: 'ASSIGNED',
    SHIPPED: 'SHIPPED'
};

const LOAD_STATES = {
    EN_ROUTE_TO_PICK_UP: 'En route to Pick Up',
    ARRIVED_TO_PICK_UP: 'Arrived to Pick Up',
    EN_ROUTE_TO_DELIVERY: 'En route to delivery',
    ARRIVED_TO_DELIVERY: 'Arrived to delivery'
};

const LOAD_STATES_TRANSITIONS = {
    'En route to Pick Up': 'Arrived to Pick Up',
    'Arrived to Pick Up': 'En route to delivery',
    'En route to delivery': 'Arrived to delivery',
    'Arrived to delivery': null
};

module.exports = {
    ROLES,
    TRUCK_TYPES,
    TRUCK_STATUSES,
    LOAD_STATES,
    LOAD_STATUSES,
    TRUCK_TYPES_SIZES,
    TRUCK_PAYLOADS,
    LOAD_STATES_TRANSITIONS
};