const Joi = require('joi');

const createTruckSchema = Joi.object({
    type: Joi.string().valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required()
});

module.exports = {
    createTruckSchema,
};