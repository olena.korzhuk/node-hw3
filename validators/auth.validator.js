const Joi = require('joi');

const registerSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().pattern(new RegExp('[a-zA-Z0-9!@#$%^&*]')).min(1).required(),
    role: Joi.string().valid('SHIPPER', 'DRIVER').required()
});

const loginSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().pattern(new RegExp('[a-zA-Z0-9!@#$%^&*]')).min(1).required(),
});

module.exports = {
    registerSchema,
    loginSchema
};