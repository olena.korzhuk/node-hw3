const Joi = require('joi');
const {NEW, ASSIGNED, POSTED, SHIPPED} = require('./../utils/constants').LOAD_STATUSES;

const createLoadSchema = Joi.object({
    name: Joi.string().required(),
    status: Joi.string().valid(NEW, POSTED, ASSIGNED, SHIPPED).default(NEW),
    state: Joi.string(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
        width: Joi.number().required(),
        length: Joi.number().required(),
        height: Joi.number().required()
    })
});

module.exports = {
    createLoadSchema,
};