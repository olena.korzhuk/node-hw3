const {createUserLoad} = require('./../services/load.service');
const loadService = require('./../services/load.service');

module.exports.getUserLoads = async (req, res) => {
    const {id: userId, role} = req.user;

    try {
        const loads = await loadService.getUserLoads(userId, role);
        res.status(200).json({
            loads: loads
        });
    } catch (e) {
        if (e.status) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Create Load: Internal server error'});
        }
    }
};

module.exports.addLoad = async (req, res) => {
    const {id: userId} = req.user;
    const load = req.body;

    try {
        await createUserLoad(userId, load);
        res.status(200).json({message: 'Load created successfully'});
    } catch (e) {
        if (e.status === 400) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Create Load: Internal server error'});
        }
    }
};

module.exports.postLoad = async (req, res) => {
    const {loadId} = req.params;

    try {
        const postedLoad = await loadService.postUserLoad(loadId);
        res.status(200).json({
            message: 'Load posted successfully',
            driver_found: !!postedLoad.assigned_to
        });
    } catch (e) {
        if (e.status === 400) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Post Load: Internal server error'});
        }
    }
};

module.exports.getLoad = async (req, res) => {
    const {loadId} = req.params;
    const {id: userId, role} = req.user;

    if (!loadId) {
        res.status(400).json({
            message: 'You should specify load id'
        });
    }

    try {
        const load = await loadService.getUserLoad(userId, loadId, role);
        res.status(200).json({
            load
        });
    } catch (e) {
        if (e.status) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Internal server error'});
        }
    }
};

module.exports.updateLoad = async (req, res) => {
    const {loadId} = req.params;
    const payload = req.body

    try {
        await loadService.updateUSerLoad(loadId, payload);
        res.status(200).json({
            message: 'Load details changed successfully'
        });
    } catch (e) {
        if (e.status) {
            res.status(e.status).json({message: e.message});
        } else {
            res.status(500).json({message: 'Internal server error'});
        }
    }
};

module.exports.getUserActiveLoad = async (req, res) => {
    const {id: userId} = req.user;

    try {
        const load = await loadService.getUserActiveLoad(userId);
        if (load) {
            res.status(200).json({
                load
            });
        } else {
            res.status(400).json({message: 'Could not find active load'});
        }
    } catch (e) {
        console.error(e)
        res.status(500).json({message: 'Get active load: Internal server error'});
    }
};

module.exports.getLoadShippingInfo = async (req, res) => {
    const {loadId} = req.params;
    const {id: userId} = req.user;

    try {
        const shippingInfo = await loadService.getLoadShippingInfo(userId, loadId);
        res.status(200).json({
            shippingInfo
        })
    } catch (e) {
        res.status(e.status).json({message: e.message});
    }
};

module.exports.moveLoadToNextState = async (req, res) => {
    const {id: userId} = req.user;

    try {
        await loadService.moveLoadToNextState(userId);
        res.status(200).json({
            message: `Load status changed to SHIPPED'`
        });
    } catch (e) {
        if (e.status) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Internal server error'});
        }
    }
};

module.exports.deleteLoad = async (req, res) => {
    const {loadId} = req.params;
    const {id: userId} = req.user;

    if (!loadId) {
        res.status(400).json({
            message: 'You should specify Id of the load'
        });
    }

    try {
        await loadService.deleteLoad(userId, loadId);
        res.status(200).json({
            message: 'Load deleted successfully'
        });
    } catch (e) {
        if (e.status) {
            res.status(e.status).json({message: e.message});
        } else {
            return res.status(500).json({message: 'Internal server error'});
        }
    }
};