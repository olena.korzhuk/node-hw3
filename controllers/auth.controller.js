const User = require('./../models/User');
const bcrypt = require('bcryptjs');
const tokenService = require('./../services/token.service');

module.exports.register = async (req, res) => {
    const {email, password, role} = req.body;

    try {
        const sameUser = await User.findOne({email});
        if (sameUser) {
            return res.status(400).json({
                message: 'User with the same email already exists'
            });
        } else {
            try {
                await User.create({
                    role,
                    email,
                    password,
                    created_at: new Date().toISOString()
                });
                res.status(200).json({
                    message: 'Profile created successfully'
                });
            } catch (e) {
                console.log('REGISTER ERROR', e);
            }
        }
    } catch (e) {
        return res.status(500).send({
            message: 'Internal Server Error'
        });
    }
};

module.exports.login = async (req, res) => {
    const {email, password} = req.body;

    try {
        const user = await User.findOne({email});
        if (!user) {
            return res.status(500).json({
                message: 'User not found'
            });
        } else {
            const isValidPassword = await bcrypt.compare(password, user.password);

            if (!isValidPassword) {
                return res.status(400).json({
                    message: 'Incorrect password or username'
                });
            }

            const token = tokenService.generateToken({username: user.email, id: user._id, role: user.role});
            res.status(200).json({
                jwt_token: token
            });
        }
    } catch (e) {
        return res.status(500).send({
            message: 'Internal Server Error'
        });
    }
};