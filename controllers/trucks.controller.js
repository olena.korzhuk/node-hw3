const Truck = require('./../models/Truck');
const User = require('./../models/User');
const truckService = require('./../services/truck.service');

module.exports.getTrucks = async (req, res) => {
    const {id} = req.user;

    try {
        const trucks = await Truck.find({created_by: id})
        res.status(200).json({
            trucks
        });
    } catch (e) {
        return res.status(500).json({message: 'Create Note: Internal server error'});
    }
};

module.exports.createTruck = async (req, res) => {
    const {id} = req.user;
    const {type} = req.body;

    try {
        const user = await User.findOne({_id: id})
        await truckService.createTruckForUser(user._id, type);
        res.status(200).json({message: 'Truck created successfully'});
    } catch (e) {
        if (e.status === 400) {
            res.status(e.status).json({
                message: e.message
            });
        } else {
            return res.status(500).json({message: 'Create Truck: Internal server error'});
        }
    }
};

module.exports.getTruck = async (req, res) => {
    const {truckId} = req.params;
    const {id: userId} = req.user;

    try {
        const truck = await Truck.findById({_id: truckId}).select('-__v');
        res.status(200).json({
            truck
        });
    } catch (e) {
        return res.status(500).json({message: 'Create Truck: Internal server error'});
    }
};

module.exports.updateTruck = async (req, res) => {
    const {truckId} = req.params;
    const {type} = req.body

    if (!truckId) {
        return res.status(400).json({
            message: 'You should specify truck id'
        });
    }

    try {
        const truck = await Truck.findOne({_id: truckId});
        if (truck) {
            await Truck.findOneAndUpdate({_id: truckId}, {type: type})
                .select('-__v');

            res.status(200).json({
                message: 'Truck details changed successfully'
            });
        } else {
            return res.status(500).json({message: 'No truck found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Update truck: Internal server error'});
    }
};

module.exports.deleteTruck = async (req, res) => {
    const {truckId} = req.params;
    const {id} = req.user;

    if (!truckId) {
        res.status(400).json({
            message: 'You should specify Id of the truck'
        });
    }

    try {
        const truck = await Truck.findOne({created_by: id, _id: truckId});
        if (truck) {
            await Truck.deleteOne({_id: truckId},)
            res.status(200).json({
                message: 'Truck deleted successfully'
            });
        } else {
            return res.status(500).json({message: 'No truck found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Delete truck: Internal server error'});
    }
};

module.exports.assignTruck = async (req, res) => {
    const {truckId} = req.params;
    const {id: userId} = req.user;

    if (!truckId) {
        res.status(400).json({
            message: 'You should specify Id of the truck'
        });
    }

    try {
        await truckService.assignTruckById(userId, truckId);
        res.status(200).json({
            message: 'Truck assigned successfully'
        });
    } catch (e) {
        if (e.status === 400) {
            res.status(e.status).json({message: e.message});
        } else {
            res.status(500).json({message: 'Assign truck: Internal server error'});
        }
    }
};