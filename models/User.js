const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// const roles = {
//     SHIPPER: 'SHIPPER',
//     DRIVER: 'DRIVER'
// };

const userSchema = new mongoose.Schema({
    role: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: {
        type: String,
        required: true
    }
});

userSchema.pre('updateOne', {document: true, query: false}, async function (next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

userSchema.pre('save', async function (next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
});

const User = mongoose.model('user', userSchema);

module.exports = User;