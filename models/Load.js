const mongoose = require('mongoose');
const {LOAD_STATUSES} = require('./../utils/constants');

const logsSchema = new mongoose.Schema({
    message: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    }
});

const dimensionsSchema = new mongoose.Schema({
    width: {
        type: Number,
        required: true
    },
    length: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    }
});

const loadSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    created_by: {
        type: String,
        required: true
    },
    assigned_to: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: LOAD_STATUSES.NEW
    },
    state: {
        type: String,
        default: ''
    },
    payload: {
        type: Number,
        required: true
    },
    pickup_address: {
        type: String,
        required: true
    },
    delivery_address: {
        type: String,
        required: true
    },
    dimensions: {
        type: dimensionsSchema
    },
    logs: {
        type: [logsSchema]
    },
    created_date: {
        type: String
    },

});

const Load = mongoose.model('load', loadSchema);

module.exports = Load;