const mongoose = require('mongoose');
// const bcrypt = require('bcryptjs');

const truckSchema = new mongoose.Schema({
    created_by: {
        type: String,
        required: true,
    },
    assigned_to: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true,
        default: 'IS'
    },
    created_date: {
        type: String,
        required: true
    }
});

const Truck = mongoose.model('truck', truckSchema);

module.exports = Truck;