const Load = require('./../models/Load');
const Truck = require('./../models/Truck');
const {CustomError} = require("../utils/CustomError");
const {ROLES} = require('./../utils/constants');
const {
    TRUCK_TYPES_SIZES,
    TRUCK_STATUSES,
    LOAD_STATUSES,
    TRUCK_PAYLOADS,
    LOAD_STATES,
    LOAD_STATES_TRANSITIONS,
} = require('./../utils/constants');

const createUserLoad = async (userId, loadPayload) => {
    const loadDoc = {
        ...loadPayload,
        created_by: userId,
        created_date: new Date().toISOString()
    }
    try {
        await Load.create(loadDoc);
    } catch (e) {
        throw new CustomError(500, 'Server Error: could not create load')
    }
};

const getUserActiveLoad = async (userId) => {
    const loads = await Load.find({
        assigned_to: userId,
        status: LOAD_STATUSES.ASSIGNED
    });
    return loads[0];
};

const postUserLoad = async (loadId) => {
    const load = await Load.findById({_id: loadId});

    if (!load) {
        throw new CustomError(400, `Load with ID ${loadId} not found`);
    }

    if (load.status !== 'NEW') {
        throw new CustomError(400, `You cannot post load in ${load.status} status. Only NEW is allowed`)
    }

    const {payload, dimensions} = load;

    try {
        await Load.findOneAndUpdate({_id: loadId}, {
            status: LOAD_STATUSES.POSTED,
            $push: {
                logs: {
                    message: `Load status changed from NEW to POSTED`,
                    time: new Date().toISOString()
                }
            }
        })

        const trucks = await Truck.find({
            status: TRUCK_STATUSES.IN_SERVICE,
            assigned_to: {$ne: ''} || {$ne: null},
        });

        const filteredTrucks = trucks.filter(truck => {
            const truckDimensions = TRUCK_TYPES_SIZES[truck.type];
            const truckPayload = TRUCK_PAYLOADS[truck.type];
            return truckPayload > payload
                && truckDimensions.width > dimensions.width
                && truckDimensions.length > dimensions.length
                && truckDimensions.height > dimensions.height;
        });

        if (filteredTrucks.length > 0) {
            const updatedTruck = await Truck.findOneAndUpdate(
                {_id: filteredTrucks[0]._id},
                {status: TRUCK_STATUSES.ON_LOAD}
            );
            await Load.findOneAndUpdate({_id: loadId}, {
                status: LOAD_STATUSES.ASSIGNED,
                assigned_to: updatedTruck.assigned_to,
                state: LOAD_STATES.EN_ROUTE_TO_PICK_UP,
                $push: {
                    logs: {
                        message: `Load assigned to driver with id ${trucks[0].assigned_to}`,
                        time: new Date().toISOString()
                    }
                }
            });
            return await Load.findOne({_id: loadId});
        } else {
            await Load.findByIdAndUpdate({_id: loadId}, {
                status: LOAD_STATUSES.NEW,
                $push: {
                    logs: {
                        message: `Could not find truck for the load with id ${loadId}`,
                        time: new Date().toISOString()
                    }
                }
            })
            return await Load.findOne({_id: loadId});
        }
    } catch (e) {
        throw new CustomError(500, 'Post load: Internal server error');
    }
};

const getUserLoads = async (userId, userRole) => {
    if (userRole === ROLES.DRIVER) {
        return await getDriverLoads(userId);
    } else {
        return await getShipperLoads(userId);
    }
};

const getUserLoad = async (userId, loadId, userRole) => {
    let foundLoad;
    if (userRole === ROLES.DRIVER) {
        try {
            foundLoad = await Load.findOne({
                _id: loadId,
                assigned_to: userId,
                status: LOAD_STATUSES.ASSIGNED || LOAD_STATUSES.SHIPPED
            });
            return foundLoad;
        } catch (e) {

        }
    } else {
        try {
            foundLoad = await Load.findOne({
                _id: loadId,
                created_by: userId
            }).select('-__v');
            return foundLoad;
        } catch (e) {
            throw new CustomError(500, 'Get Load: could not find load')
        }
    }
};

const updateUSerLoad = async (loadId, payload) => {
    let load = await Load.findOne({_id: loadId});

    if (!load) {
        throw new CustomError(400, 'Could not find load');
    }

    try {
        await Load.findOneAndUpdate({_id: loadId}, payload).select('-__v');
    } catch (e) {
        throw new CustomError(500, 'Could not update load');
    }
};

const getDriverLoads = async (userId) => {
    try {
        return await Load.find({
            assigned_to: userId,
            $or: [{status: LOAD_STATUSES.ASSIGNED}, {status: LOAD_STATUSES.SHIPPED}]
        }).select('-__v');
    } catch (e) {
        throw new CustomError(500, 'Server Error: could not get loads for user')
    }
};

const getShipperLoads = async (userId) => {
    try {
        return await Load.find({
            created_by: userId,
        }).select('-__v');
    } catch (e) {
        throw new CustomError(500, 'Server Error: could not get loads for user')
    }
};

const getLoadShippingInfo = async (userId, loadId) => {
    let load = await Load.findOne({created_by: userId, _id: loadId}).select('-__v');

    if (!load) {
        throw new CustomError(500, 'Could not find load');
    }

    try {
        const truck = await Truck.findOne({
            assigned_to: load.assigned_to,
            status: TRUCK_STATUSES.ON_LOAD
        }).select('-__v');
        return {load, truck};
    } catch (e) {
        throw new CustomError(500, 'Get load shipping info: Internal server error');
    }
};

const deleteLoad = async (userId, loadId) => {
    let load = await Load.findOne({created_by: userId, _id: loadId}).select('-__v');

    if (!load) {
        throw new CustomError(500, 'Could not find load');
    }

    try {
        await Load.deleteOne({_id: loadId},)
    } catch (e) {
        return new CustomError(500, 'Delete load: Internal server error');
    }
};

const moveLoadToNextState = async (userId) => {
    let load = await Load.findOne({assigned_to: userId, status: LOAD_STATUSES.ASSIGNED});

    if (!load) {
        throw new CustomError(500, 'Could not find load');
    }

    try {
        const {state: currentState} = load;
        const nextState = LOAD_STATES_TRANSITIONS[currentState];

        if (nextState === LOAD_STATES.ARRIVED_TO_DELIVERY) {
            load.status = LOAD_STATUSES.SHIPPED;
            load.logs.push({
                message: `Load status changed to ${LOAD_STATUSES.SHIPPED}`,
                time: new Date().toISOString()
            });

            await load.save();
            await Truck.findOneAndUpdate({
                assigned_to: load.assigned_to,

            }, {
                status: TRUCK_STATUSES.IN_SERVICE
            });
        }
        load.state = nextState;
        load.logs.push({
            message: `Load state changed from ${currentState} to ${nextState}`,
            time: new Date().toISOString()
        });
        await load.save();
    } catch (e) {
        return new CustomError(500, 'Update load state: Internal server error');
    }
};

module.exports = {
    createUserLoad,
    getUserLoads,
    getUserLoad,
    postUserLoad,
    updateUSerLoad,
    getUserActiveLoad,
    getShipperLoads,
    getDriverLoads,
    getLoadShippingInfo,
    deleteLoad,
    moveLoadToNextState
};