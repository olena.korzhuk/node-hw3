const Truck = require('./../models/Truck');
const User = require("../models/User");
const {CustomError} = require('./../utils/CustomError');

const assignTruckById = async (userId, truckId) => {
    const trucks = await getTruckAssignedToUser(userId);

    if (trucks.length === 0) {
        await Truck.findOne({_id: truckId}).updateOne({assigned_to: userId})
    } else {
        throw new CustomError(400, 'User already has a truck assigned')
    }
};

const getTruckAssignedToUser = (userId) => {
    return Truck.find({assigned_to: userId});
};

const findUserById = (userId) => {
    return User.findOne({_id: userId});
};

const createTruckForUser = async (userId, truckType) => {
    const newTruck = {
        created_by: userId,
        type: truckType,
        assigned_to: "",
        created_date: new Date().toISOString()
    }

    try {
        await Truck.create(newTruck);
    }catch (e) {
        console.error('CREATE TRUCK ERROR', e);
       throw new CustomError(500, 'Server Error: could not create truck')
    }
};

module.exports = {
    assignTruckById,
    createTruckForUser
};