const {Router} = require('express');

const usersController = require('./../controllers/users.controller');

const router = Router();

router.get('/me', usersController.getUserInfo);

router.patch('/me/password', usersController.changePassword);

router.delete('/me', usersController.deleteUserProfile);

module.exports = router;