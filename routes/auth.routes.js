const {Router} = require('express');
const authController = require('./../controllers/auth.controller');
const {registerSchema, loginSchema} = require('./../validators/auth.validator');
const {getValidateMiddleware} = require('./../middleware/auth.middleware');

const router = Router();

router.post('/register', getValidateMiddleware(registerSchema), authController.register);
router.post('/login', getValidateMiddleware(loginSchema), authController.login);

module.exports = router;