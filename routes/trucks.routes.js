const {Router} = require('express');
const truckController = require('./../controllers/trucks.controller');
const {isDriver} = require("../middleware/driver.middleware");
const {createTruckSchema} = require('./../validators/truck.validator');
const {getValidateMiddleware} = require('./../middleware/auth.middleware');

const router = Router();

router.get('/', isDriver, truckController.getTrucks);
router.get('/:truckId', isDriver, truckController.getTruck);
router.post('/', getValidateMiddleware(createTruckSchema), truckController.createTruck);
router.post('/:truckId/assign', isDriver, truckController.assignTruck);
router.put('/:truckId', isDriver, truckController.updateTruck);
router.delete('/:truckId', isDriver, truckController.deleteTruck);

module.exports = router;