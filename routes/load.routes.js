const {Router} = require('express');
const loadController = require('./../controllers/load.controller');
const {isShipper} = require("../middleware/shipper.middleware");
const {isDriver} = require("../middleware/driver.middleware");
const {createLoadSchema} = require('./../validators/load.validator');
const {getValidateMiddleware} = require('./../middleware/auth.middleware');

const router = Router();

router.get('/', loadController.getUserLoads);
router.get('/active', isDriver, loadController.getUserActiveLoad);
router.get('/:loadId', loadController.getLoad);
router.patch('/active/state', isDriver, loadController.moveLoadToNextState);
router.post('/', isShipper, getValidateMiddleware(createLoadSchema), loadController.addLoad);
router.post('/:loadId/post', isShipper, loadController.postLoad);
router.put('/:loadId', isShipper, loadController.updateLoad);
router.delete('/:loadId', isShipper, loadController.deleteLoad);
router.get('/:loadId/shipping_info', isShipper, loadController.getLoadShippingInfo);

module.exports = router;