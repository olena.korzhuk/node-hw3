const jwt = require('jsonwebtoken');

const handleClientValidationError = (res, e) => {
    res.status(400).json({
        message: `Validation error: ${e.message}`
    });
};

module.exports.authVerifyToken = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(400).json({message: 'Access token is not provided for the request'});
    }

    const accessToken = token.split(' ')[1];
    try {
        const verified = jwt.verify(accessToken, process.env.JWT_SECRET);
        req.user = {id: verified.id, username: verified.username, role: verified.role};
        next();
    } catch (e) {
        return res.status(400).json({message: 'Invalid access token'});
    }
};

module.exports.getValidateMiddleware = (schema) => {
    return async function (req, res, next) {
        try {
            await schema.validateAsync(req.body);
            next();
        } catch (e) {
            handleClientValidationError(res, e);
        }
    }
}