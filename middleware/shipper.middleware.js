const {ROLES} = require('./../utils/constants');

module.exports.isShipper = (req, res, next) => {
    if (req.user.role === ROLES.SHIPPER) {
        next();
    } else {
        res.status(400).json({
            message: `You are not authorized to perform this action`
        });
    }
}