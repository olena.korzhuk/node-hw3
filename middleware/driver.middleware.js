const {ROLES} = require('./../utils/constants');

module.exports.isDriver = (req, res, next) => {
    if (req.user.role === ROLES.DRIVER) {
        next();
    } else {
        res.status(400).json({
            message: `You are not authorized to perform this action`
        });
    }
}